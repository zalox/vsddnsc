# VSDDNSC

## Very simple dynamic DNS client for https://dns.he.net

VSDDNSC makes it simple to dynamicaly update your record at Hurricane Electrics DNS service.

## Requirements

```
bash
dig
curl
```

## Installation

```sh
git clone https://gitlab.com/zalox/vsddnsc
cd vsddnsc
chmod +x ./vsddnsc
sudo cp vsddnsc /bin/vsddnsc
```

## Usage

```sh
vsddnsc example.com <password>
# or
export VSDDNSC_PASSWORD=<password>
vsddnsc example.com
```
